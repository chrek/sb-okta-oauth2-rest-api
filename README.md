# Angular7Bootstrap4

This project was generated with [Spring Initializr](https://start.spring.io/) version 2.1.2.

The Spring Boot project is used to create a secured REST API based on Spring Security and Okta.
OAuth 2.0 Scopes are used in addition.
 
## Development server
Run `mvnw spring-boot:run` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.

## Useful information
* For your OAuth 2.0 Resource Server, create an Application of type Service in Okta site: This will give you a Client ID and a Client Secret.
* Create a Web application in Okta inorder to have a ClientID that will be used when generating Tokens from https://oidcdebugger.com/debug  site
* Generate Tokens from https://oidcdebugger.com/debug  site using the ClientID already generated when creating a Web application in Okta


## References
1. [Javacodegeeks](https://www.javacodegeeks.com/2019/01/create-secure-spring-rest-api.html).
2. [Okta Developer](https://developer.okta.com/blog/2017/12/18/spring-security-5-oidc).
3. [Okta Devforum](https://devforum.okta.com/t/okta-spring-boot-starter-1-0-released/3728).

