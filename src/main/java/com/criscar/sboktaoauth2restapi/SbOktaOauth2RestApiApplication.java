package com.criscar.sboktaoauth2restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbOktaOauth2RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbOktaOauth2RestApiApplication.class, args);
	}

}

