package com.criscar.sboktaoauth2restapi.controllers;

import java.security.Principal;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@GetMapping("/")
	String home(Principal user) {
		return "Hello " + user.getName();
	}

	@PreAuthorize("#oauth2.hasScope('profile')")
	@GetMapping("/protected/")
	public String helloWorldProtected(Principal principal) {
		return "Hello VIP " + principal.getName();
	}

}
